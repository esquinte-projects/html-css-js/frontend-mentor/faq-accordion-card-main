/**
 * Toggle the arrow (up/down)
 * @param {*} question
 */
function toggleArrow(question) {
	const arrow = question.parentElement.querySelector('i');
	arrow.classList.toggle('collapsed-arrow');
	arrow.classList.toggle('expanded-arrow');
}

/**
 * Toogle the state of 'clicked question'
 * @param {*} question
 */
function toggleClickedQuestion(question) {
	question.classList.toggle('question-clicked');
}

/**
 * Toggle the response (show/hide)
 * @param {*} question
 */
function toggleResponse(question) {
	const response = question.parentElement.parentElement.querySelector('.response');
	if (response.style.display === 'none') {
		response.style.display = 'block';
	} else {
		response.style.display = 'none';
	}
}

const questions = document.querySelectorAll('.clickable-question');
questions.forEach((question) => {
	question.addEventListener('click', (event) => {
		const question = event.currentTarget.querySelector('.question');

		if (!question.classList.contains('question-clicked')) {
			const questionClicked = document.querySelector('.question-clicked');

			if (questionClicked) {
				toggleClickedQuestion(questionClicked);
				toggleResponse(questionClicked);
				toggleArrow(questionClicked);
			}
		}
		toggleClickedQuestion(question);
		toggleResponse(question);
		toggleArrow(question);
	});
});

/**
 * Initialize all responses display
 */
(function init() {
	const responses = document.querySelectorAll('.response');
	responses.forEach((response) => {
		response.style.display = 'none';
	});
})();
